module.exports = {
    messageType: "email",
    subject: "Reset Your Password",
    template: "personal",
    to: {
        // type: "contact",
        // id: "57d9a5dbf044c22ecbd1fe55"
        type:'user',
        user_id: 'Carolina'
    },
    templateUrl: 'test/fixtures/messages/example.tpl.html',
    templateVars:{
        server : 'eya',
        url : 'prueba',
        email: 'esta'
    }
};