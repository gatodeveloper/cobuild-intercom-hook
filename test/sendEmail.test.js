'use strict';

var credentials = require('./fixtures/credentials');
var should = require('should');

describe('intercom send template email', function() {

    it('Should send a email with template', function (done) {
        this.timeout(5000);

        var intercomHook = require('../index')(credentials);

        var message = require('./fixtures/messages/sendTemplate');

        intercomHook.sendEmailTemplate(message)
            .then(function(response){
                response.should.ok();
                done();
            })
            .catch(function(err){
                should.not.exist(err);
            });

    });


});

