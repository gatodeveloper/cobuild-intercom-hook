'use strict';

var credentials = require('./fixtures/credentials');
var should = require('should');

describe('intercom operations users', function() {

    var intercomHook = require('../index')(credentials);

    // it('Should create user', function (done) {
    //
    //     this.timeout(5000);
    //
    //     intercomHook.createUser({email: 'jayne@example.io'})
    //         .then(function(user){
    //             console.log(user);
    //             done();
    //         })
    //         .catch(function(err){
    //             done(err);
    //         });
    //
    // });

    // it('Should delete users', function (done) {
    //
    //     this.timeout(5000);
    //
    //     intercomHook.deleteUser({id:'57da9fe921a34840dfe47c74'})
    //         .then(function(user){
    //             console.log(user);
    //             done();
    //         })
    //         .catch(function(err){
    //             done(err);
    //         });
    //
    // });

    it('Should list users', function (done) {

        this.timeout(5000);

        intercomHook.listUsers()
            .then(function(list){
                console.log(list);
                done();
            })
            .catch(function(err){
                done(err);
            });

    });


});

