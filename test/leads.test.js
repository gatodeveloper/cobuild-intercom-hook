'use strict';

var credentials = require('./fixtures/credentials');
var should = require('should');

describe('intercom operations leads', function() {

    var intercomHook = require('../index')(credentials);

    // it('Should create lead', function (done) {
    //
    //     this.timeout(5000);
    //
    //     intercomHook.createLead({email: 'cruz.torrez@gmail.com'})
    //         .then(function(lead){
    //             console.log(lead);
    //             done();
    //         })
    //         .catch(function(err){
    //             done(err);
    //         });
    //
    // });

    // it('Should delete lead', function (done) {
    //
    //     this.timeout(5000);
    //
    //     intercomHook.deleteLead({id:'57d8eba521a348f840b757d7'})
    //         .then(function(lead){
    //             console.log(lead);
    //             done();
    //         })
    //         .catch(function(err){
    //             console.log(err);
    //             done(err);
    //         });
    //
    // });

    // it('Should convert lead', function (done) {
    //
    //     this.timeout(5000);
    //
    //     intercomHook.listLeads()
    //         .then(function(list){
    //             console.log(list);
    //             done();
    //         })
    //         .catch(function(err){
    //             done(err);
    //         });
    //
    // });

    // it('Should convert lead', function (done) {
    //
    //     this.timeout(5000);
    //
    //     intercomHook.convertLead({
    //         contact: { user_id: '6ffc2cbb-7d1f-438f-a1ed-8d1c2df5eec4'},
    //         user: { email: 'camilo@rokk3rlabs.com'}
    //     })
    //         .then(function(list){
    //             console.log(list);
    //             done();
    //         })
    //         .catch(function(err){
    //             done(err);
    //         });
    // });


    it('Should list leads', function (done) {

        this.timeout(5000);

        intercomHook.listLeads()
            .then(function(list){
                console.log(list);
                done();
            })
            .catch(function(err){
                done(err);
            });
    });

});

