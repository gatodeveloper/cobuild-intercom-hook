'use strict';

/**
 * This is a simple hook that improves the functionality of the actual service of intercom.
 *
 * @author Carolina Guerrero <carolina.guerrero@rokk3rlabs.com>
 * @version 0.0.1
 */

var Intercom = require('intercom-client'),
    util = require('./util'),
    async = require('async'),
    handlebars = require('handlebars'),
    q = require('q'),
    fs = require('fs'),
    _ = require('lodash');


module.exports = function intercomHook(credentials) {

    /*************************** Messages functions ****************************************/

    var messageTemplate = require('./json_structure/message.json');

    messageTemplate.from.id = credentials.from.id;
    messageTemplate.from.type = credentials.from.type;
    //var client =  new Intercom.Client({ token: credentials.token });
    var client =  new Intercom.Client(credentials.appId, credentials.apiKey);

    /**
     * Send the email from admin to contact or user
     *
     * @param  {Object}   data
     *
     */

    function sendEmailTemplate(data) {

        var defer = q.defer();
        var message = _buildMessage(data);

        if(_.isEmpty(message)) {
            defer.reject('Incorrect parameters. Please check the documentation.');
        }else{
            var templateFile = data.templateUrl;
            if(templateFile) {
                if(fs.existsSync(templateFile)){

                    var source = fs.readFileSync(templateFile, 'utf8')
                    var template = handlebars.compile(source);
                    var html = template(data.templateVars);
                    message.body = html;

                    client.messages.create(message)
                        .then(function(response){
                            defer.resolve(response);
                        })
                        .catch(function(err){
                            defer.reject(err);
                        });

                }else{
                    defer.reject('Mail template not found!');
                }
            }else{
                defer.reject('Mail template not found!');
            }
        }

        return defer.promise;

    }

    /*************************** Lead functions ****************************************/

    function listLeads(){
        var defer = q.defer();
        client.leads.list()
            .then(function(list){
                defer.resolve(list.body);
            })
            .catch(function(err){
                defer.reject(err);
            });
        return defer.promise;
    }

    function createLead(data){
        var defer = q.defer();
        client.leads.create(data)
            .then(function(response){
                defer.resolve(response.body);
            })
            .catch(function(err){
                defer.reject(err);
            });
        return defer.promise;
    }

    function deleteLead(data){
        var defer = q.defer();
        client.leads.delete(data)
            .then(function(response){
                defer.resolve(response.status);
            })
            .catch(function(err){
                err = JSON.parse(err.message);
                var msg = err.body.errors[0].message;
                var status = err.statusCode;
                defer.reject({status: status, message: msg});
            });
        return defer.promise;
    }

    function convertLead(convertion){
        var defer = q.defer();
        client.leads.convert(convertion)
            .then(function(response){
                defer.resolve(response.status);
            })
            .catch(function(err){
                err = JSON.parse(err.message);
                var msg = err.body.errors[0].message;
                var status = err.statusCode;
                defer.reject({status: status, message: msg});
            });
        return defer.promise;
    }

    /*************************** User functions ****************************************/

    function listUsers(){
        var defer = q.defer();
        client.users.list()
            .then(function(response){
                defer.resolve(response.body);
            })
            .catch(function(err){
                defer.reject(err);
            });
        return defer.promise;
    }

    function createUser(data){
        var defer = q.defer();
        client.users.create(data)
            .then(function(response){
                defer.resolve(response.body);
            })
            .catch(function(err){
                defer.reject(err);
            });
        return defer.promise;
    }

    function deleteUser(data){
        var defer = q.defer();
        client.leads.delete(data)
            .then(function(response){
                defer.resolve(response.status);
            })
            .catch(function(err){
                err = JSON.parse(err.message);
                var msg = err.body.errors[0].message;
                var status = err.statusCode;
                defer.reject({status: status, message: msg});
            });
        return defer.promise;
    }

    /*************************** Private functions ****************************************/

    /**
     * Build the structure of message for each email send
     *
     * @param  {Object} paramsToSend
     * @return {Object}
     */

    function _buildMessage(paramsToSend) {

        var message = {};
        paramsToSend.from = messageTemplate.from;

        if(_.isEmpty(paramsToSend)) {
            return message;
        }

        //Check if the structure is valid
        if(!paramsToSend.messageType || !paramsToSend.subject || !paramsToSend.template || !paramsToSend.from || !paramsToSend.to){
            return message;
        }

        //Check if the transmitter is valid
        if(!paramsToSend.from.type || !paramsToSend.from.id){
            return message;
        }

        //Check if the receptor is valid
        if(!paramsToSend.to.type || !(paramsToSend.to.id || paramsToSend.to.user_id || paramsToSend.to.email)){
            return message;
        }

        //Building the message with passed parameters
        message = _.clone(messageTemplate);
        message.message_type = paramsToSend.messageType;
        message.subject = paramsToSend.subject;
        message.body = paramsToSend.body;
        message.template = paramsToSend.template;
        message.from = paramsToSend.from;
        message.to = paramsToSend.to;

        return message;
    }

    function compile(variables, dir) {
        var templateExist = false;
        console.log(fs.existsSync(dir));
        if(fs.existsSync(dir)){
            templateExist = true;
        }

        if(templateExist){
            var data = fs.readFileSync(dir, 'utf8')
            var template = handlebars.compile(data);
            var html = template([]);
            console.log("Si tiene");
            return  html;
        }else{
            return 'Mail template ' + templateFile + ' not found!';
        }
    }

    return {
        sendEmailTemplate: sendEmailTemplate,
        listLeads: listLeads,
        createLead: createLead,
        deleteLead: deleteLead,
        convertLead: convertLead,
        listUsers: listUsers,
        deleteUser: deleteUser,
        createUser: createUser
    };

};